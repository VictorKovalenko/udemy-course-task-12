import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MyProvider(),
      child: MaterialApp(
        title: 'Task12',
        theme: ThemeData.dark(),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int number = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(''),
      ),
      body: Column(
        children: [
          Consumer<MyProvider>(
            builder: (context, value, _) {
              return Container(
                height: 300,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: value.number,
                  itemBuilder: (context, index) => Container(
                    width: 300,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      border: Border.all(width: 10),
                    ),
                  ),
                ),
              );
            },
          ),

          Row(
            children: [
              Expanded(
                child: Container(),
              ),
              ElevatedButton(
                onPressed: () {
                  setState(() => Provider.of<MyProvider>(context, listen: false).changeNumber(--number));
                },
                child: Text('-'),
              ),
              Expanded(
                child: Container(),
              ),
              ElevatedButton(
                onPressed: () {
                  setState(() => Provider.of<MyProvider>(context, listen: false).changeNumber(++number));
                },
                child: Text('+'),
              ),
              Expanded(
                child: Container(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class MyProvider extends ChangeNotifier {
  int _number = 0;

  int get number => _number;

  void changeNumber(int value) {
    _number = value;
    print(number);
    notifyListeners();
  }
}
